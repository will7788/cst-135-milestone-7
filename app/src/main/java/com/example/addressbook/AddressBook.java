package com.example.addressbook;

import java.util.*;

public class AddressBook {

    public ArrayList<BaseContact> contacts;

    public AddressBook() {

    }

    public AddressBook(ArrayList<BaseContact> contacts) {
        this.setContacts(contacts);
    }

    public ArrayList<BaseContact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<BaseContact> contacts) {
        this.contacts = contacts;
    }

    public void add(BaseContact contact)
    {
        contacts.add(contact);
    }

    public void remove(Object contact)
    {
        contacts.remove(contact);
    }

    public void display(BaseContact contact)
    {
        System.out.println(contact.toString());
    }

    public void sort()
    {

    }

    public BaseContact Search(int number)
    {
        for (BaseContact contact : contacts)
        {
            if (contact.getNumber() == number)
            {
                return contact;
            }
        }
        return null;
    }
    public BaseContact Search(String name)
    {
        for (BaseContact contact : contacts)
        {
            if (contact.getName().equals(name))
            {
                return contact;
            }
        }
        return null;
    }

    public PersonContact Search(ArrayList<PersonContact> relatives)
    {
        for (BaseContact contact : contacts)
        {
            PersonContact temp = (PersonContact)contact;
            if (temp.getRelatives() == relatives)
            {
                return temp;
            }
        }
        return null;
    }

    public PersonContact Search(Location location)
    {
        for (BaseContact contact : contacts)
        {
            PersonContact temp = (PersonContact)contact;
            if (temp.getLocation() == location)
            {
                return temp;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "AddressBook Contacts: " + contacts.toString();
    }



}

