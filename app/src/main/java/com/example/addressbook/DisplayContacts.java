package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DisplayContacts extends AppCompatActivity {

    ListView lv_listofnames;
    ArrayAdapter ad;
    List<String> friends = new ArrayList<String>();
    String[] startingList = {"Jeffrey Brown", "John Jones", "William Thornton", "Derek Walters"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contacts);

        lv_listofnames = findViewById(R.id.listView);

        friends = new ArrayList<String>(Arrays.asList(startingList));
        ad = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friends);

        lv_listofnames.setAdapter(ad);
    }
}
